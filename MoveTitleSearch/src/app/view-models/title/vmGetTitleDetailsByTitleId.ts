import {TitleWithDetails} from "../../models/Titles/titleWithDetails";

export class VmGetTitleDetailsByTitleId {
  public Title: TitleWithDetails;
}
