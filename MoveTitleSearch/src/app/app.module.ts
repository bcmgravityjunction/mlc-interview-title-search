import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TitleSearchComponent } from './title-search/title-search.component';
import {FormsModule} from "@angular/forms";
import { TitleDetailComponent } from './title-detail/title-detail.component';
import {MatDialogModule} from "@angular/material";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {TitleDataServiceService} from "./services/data-services/title-data-service.service";
import {AppConfigService} from "./services/app-services/app-config-service";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    TitleSearchComponent,
    TitleDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatDialogModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  entryComponents:[
    TitleDetailComponent
  ],
  providers: [
    TitleDataServiceService,
    AppConfigService,
    HttpClientModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
