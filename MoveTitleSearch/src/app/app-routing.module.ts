import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TitleSearchComponent} from "./title-search/title-search.component";


const routes: Routes = [
  {path: '', component: TitleSearchComponent},
  {path: 'title-search', component: TitleSearchComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
