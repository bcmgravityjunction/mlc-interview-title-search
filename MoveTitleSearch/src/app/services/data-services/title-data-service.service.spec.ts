import { TestBed } from '@angular/core/testing';

import { TitleDataServiceService } from './title-data-service.service';

describe('TitleDataServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TitleDataServiceService = TestBed.get(TitleDataServiceService);
    expect(service).toBeTruthy();
  });
});
