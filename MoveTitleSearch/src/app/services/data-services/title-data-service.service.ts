import { Injectable } from '@angular/core';
import {BaseDataService} from "./base-data-service";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {JsonWrapper} from "../../models/Common/json-wrapper";
import {VmGetTitlesByNameSearch} from "../../view-models/title/VmGetTitlesByNameSearch";
import {FmGetTitlesByNameSearch} from "../../form-models/title/fmGetTitlesByNameSearch";
import {FmGetTitleDetailsByTitleId} from "../../form-models/title/fmGetTitleDetailsByTitleId";
import {VmGetTitleDetailsByTitleId} from "../../view-models/title/vmGetTitleDetailsByTitleId";

@Injectable({
  providedIn: 'root'
})
export class TitleDataServiceService extends BaseDataService{

  constructor(public http: HttpClient) {
    super(http, '/Titles');
  }

  getTitlesByNameSearch(fmGetTitlesByNameSearch: FmGetTitlesByNameSearch) {
    const methodPath = '/GetTitlesByNameSearch';
    return this.postApi(this.baseApiUrl + this.apiControllerPath + methodPath, fmGetTitlesByNameSearch)
      .pipe(map(res => res as JsonWrapper<VmGetTitlesByNameSearch>));
  }

  getTitleDetailsByTitleId(fmGetTitleDetailsByTitleId: FmGetTitleDetailsByTitleId) {
    const methodPath = '/GetTitleDetailsByTitleId';
    return this.postApi(this.baseApiUrl + this.apiControllerPath + methodPath, fmGetTitleDetailsByTitleId)
      .pipe(map(res => res as JsonWrapper<VmGetTitleDetailsByTitleId>));
  }
}
