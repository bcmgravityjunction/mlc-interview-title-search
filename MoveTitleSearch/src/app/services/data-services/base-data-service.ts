
import {HttpClient, HttpHeaders, HttpRequest  } from '@angular/common/http';
import {AppConfigService} from "../app-services/app-config-service";


export class BaseDataService {
    protected headers: HttpHeaders;
    protected baseApiUrl: string;

    constructor(public http: HttpClient, protected apiControllerPath: string) {
        this.headers = new HttpHeaders();
        this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');
        /*AppConfigService.getConfig(http).subscribe(data => {
          this.baseApiUrl = data['api-base-url'];
        });*/
        this.baseApiUrl = AppConfigService.BaseURL;
    }

    postApi(url: string, sendData: any) {
        return this.http.post(url, sendData, {headers : this.headers});
    }
}
