import {Injectable} from "@angular/core";

@Injectable()
export class AppConfigService {
  public static BaseURL = window.location.protocol + "//" + window.location.host + "/api" ;
}
