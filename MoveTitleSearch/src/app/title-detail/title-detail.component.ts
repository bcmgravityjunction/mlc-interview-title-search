import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import {TitleDataServiceService} from "../services/data-services/title-data-service.service";
import {FmGetTitleDetailsByTitleId} from "../form-models/title/fmGetTitleDetailsByTitleId";
import {VmGetTitleDetailsByTitleId} from "../view-models/title/vmGetTitleDetailsByTitleId";
import {TitleWithDetails} from "../models/Titles/titleWithDetails";
@Component({
  selector: 'app-title-detail',
  templateUrl: './title-detail.component.html',
  styleUrls: ['./title-detail.component.scss']
})
export class TitleDetailComponent implements OnInit {

  titleWithDetails: TitleWithDetails = new TitleWithDetails();
  errorMessage = "";
  private isLoading = false;
  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private titlesDataService: TitleDataServiceService
  ) {
    const fmGetTitleDetailsByTitleId = new FmGetTitleDetailsByTitleId();
    fmGetTitleDetailsByTitleId.Id = data;
    this.isLoading = true;
    this.titlesDataService.getTitleDetailsByTitleId(fmGetTitleDetailsByTitleId).subscribe((data)=>{
      if(data.IsSuccess){
        this.titleWithDetails = data.Data.Title;
        this.isLoading = false;
      }else{
        this.errorMessage = "Error Code: " + data.Error.ErrorCode + " : " + data.Error.ErrorMessage;
        this.isLoading = false;
      }
    }, error => {
      this.isLoading = false;
      this.errorMessage = "Error.  Please try again later";
    });

  }

  ngOnInit() {
    console.log(this.data);
  }

}
