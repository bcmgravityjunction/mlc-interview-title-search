import { Component, OnInit } from '@angular/core';
import {Subject, Subscription} from "rxjs";
import { debounceTime, map } from 'rxjs/operators';
import {VmGetTitlesByNameSearch} from "../view-models/title/VmGetTitlesByNameSearch";
import {MatDialog, MatDialogConfig, MatDialogRef} from "@angular/material";
import {TitleDetailComponent} from "../title-detail/title-detail.component";
import {TitleDataServiceService} from "../services/data-services/title-data-service.service";
import {FmGetTitlesByNameSearch} from "../form-models/title/fmGetTitlesByNameSearch";
import {Title} from "../models/Titles/title";

@Component({
  selector: 'app-title-search',
  templateUrl: './title-search.component.html',
  styleUrls: ['./title-search.component.scss']
})
export class TitleSearchComponent implements OnInit {

  private titleSearchSub  = new Subject<any>();
  private searchText: string;
  private searchResults = new Array<Title>();
  private errorToShow: string;
  private isLoading = false;
  constructor(private dialog: MatDialog, private titleDataService:TitleDataServiceService) { }

  ngOnInit() {


    this.titleSearchSub.pipe(
      debounceTime(500)
    ).subscribe((textToSearch)=>{
      if(textToSearch){
        this.isLoading = true;
        const fmGetTitlesByNameSearch = new FmGetTitlesByNameSearch();
        fmGetTitlesByNameSearch.TextToSearch = textToSearch;
        this.titleDataService
          .getTitlesByNameSearch(fmGetTitlesByNameSearch)
          .subscribe((data)=>{
            if(data.IsSuccess){
              this.errorToShow = null;
              this.searchResults = data.Data.Titles;
            }else{
              this.errorToShow = "Error Code: " + data.Error.ErrorCode + " : " + data.Error.ErrorMessage
            }
            this.isLoading = false;
          },error => {
            this.isLoading = false;
            this.errorToShow = "Error.  Please try again later";
          });
      }else{
          this.errorToShow = null;
          this.searchResults = new Array<Title>();
      }
    });
  }

  onSearchTextChanged($event){
    this.titleSearchSub.next($event.target.value);
  }

  onShowDetailsClicked(Id: number){
    const dialogRef = this.dialog.open(TitleDetailComponent, {
      width: '80%',
      data: Id
    });
  }


}
