import {ApiError} from "./api-error";

export class JsonWrapper<T> {
  public Data: T;
  public Error: ApiError;
  public IsSuccess: boolean;
}
