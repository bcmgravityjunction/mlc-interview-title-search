export class TitleAward {
  public TitleId: number;
  public AwardWon: boolean;
  public AwardYear: number;
  public Award: string;
  public AwardCompany: string;
}
