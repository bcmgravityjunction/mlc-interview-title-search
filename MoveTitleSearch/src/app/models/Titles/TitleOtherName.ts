export class TitleOtherName {
  public TitleNameLanguage: string;
  public TitleNameType: string;
  public TitleNameSortable: string;
  public TitleName: string;
}
