export class Title {
  public Name: string;
  public Id: number;
  public StoryLine: string;
  public ReleaseYear: number;
}
