export class TitleParticipant {
  public Name: string;
  public ParticipantType: string;
  public IsKey: boolean;
  public RoleType: string;
  public IsOnScreen: boolean;
}
