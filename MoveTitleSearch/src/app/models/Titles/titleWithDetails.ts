import {TitleGenre} from "./TitleGenre";
import {TitleAward} from "./TitleAward";
import {TitleOtherName} from "./TitleOtherName";
import {TitleParticipant} from "./TitleParticipant";
import {TitleStoryLine} from "./TitleStoryLine";

export class TitleWithDetails {
  public Name: string;
  public Id: number;
  public Genres: Array<TitleGenre>;
  public Awards: Array<TitleAward>;
  public OtherNames: Array<TitleOtherName>;
  public Participants: Array<TitleParticipant>;
  public StoryLines: Array<TitleStoryLine>;
  public ReleaseYear: number;
}
