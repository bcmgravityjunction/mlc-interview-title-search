﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoviesAPI.BLL.Titles.Models;

namespace MoviesAPI.ViewModels.Titles
{
    public class VmGetTitleDetailsByTitleId
    {
        public TitleWithDetails Title { get; set; }
    }
}
