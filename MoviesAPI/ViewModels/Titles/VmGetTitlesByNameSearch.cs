﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;
using MoviesAPI.BLL.Titles.Models;

namespace MoviesAPI.ViewModels.Titles
{
    public class VmGetTitlesByNameSearch
    {
        public IList<Title> Titles { get; set; }
    }
}
