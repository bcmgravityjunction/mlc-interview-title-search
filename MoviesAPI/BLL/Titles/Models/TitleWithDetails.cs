﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.BLL.Titles.Models
{
    public class TitleWithDetails
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public IList<TitleGenre> Genres { get; set; }
        public IList<TitleAward> Awards { get; set; }

        public IList<TitleOtherName> OtherNames { get; set; }
        public IList<TitleParticipant> Participants { get; set; }
        public IList<TitleStoryLine> StoryLines { get; set; }
        public int? ReleaseYear { get; set; }
    }
}
