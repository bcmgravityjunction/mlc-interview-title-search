﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.BLL.Titles.Models
{
    public class TitleGenre
    {
        public string Name { get; set; }
    }
}
