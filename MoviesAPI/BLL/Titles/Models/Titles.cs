﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.BLL.Titles.Models
{
    public class Title
    {
        public string Name { get; set; }
        public int Id { get; set; }

        public string StoryLine { get; set; }
        public int? ReleaseYear { get; set; }
    }
}
