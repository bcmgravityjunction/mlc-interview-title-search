﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.BLL.Titles.Models
{
    public class TitleStoryLine
    {
        public string Type { get; set; }
        public string Language { get; set; }
        public string Description { get; set; }
    }
}
