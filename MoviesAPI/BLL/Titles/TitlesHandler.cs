﻿using System;
using System.Collections.Generic;
using System.Linq;
using MoviesAPI.BLL.Titles.Models;
using MoviesAPI.Common;
using MoviesAPI.DAL;
using MoviesAPI.FormModels.Titles;
using MoviesAPI.ViewModels.Titles;
using Title = MoviesAPI.BLL.Titles.Models.Title;
using TitleGenre = MoviesAPI.BLL.Titles.Models.TitleGenre;
using TitleParticipant = MoviesAPI.BLL.Titles.Models.TitleParticipant;

namespace MoviesAPI.BLL.Titles
{
    public class TitlesHandler
    {
        private TitlesContext _dbTeTitlesContext;
        public TitlesHandler(TitlesContext titlesContext)
        {
            _dbTeTitlesContext = titlesContext;
        }

        public JsonWrapper<VmGetTitlesByNameSearch> GetTitlesByNameSearch(FmGetTitlesByNameSearch requestFmGetTitlesByNameSearch)
        {
            JsonWrapper<VmGetTitlesByNameSearch> returnObject = new JsonWrapper<VmGetTitlesByNameSearch>();
            returnObject.Error = new ApiError();
            returnObject.IsSuccess = false;

            if (string.IsNullOrEmpty(requestFmGetTitlesByNameSearch.TextToSearch))//Error code 10021
            {
                returnObject.Error.ErrorCode = "10021";
                returnObject.Error.ErrorMessage = "No search text was given. Please enter text to search";
                return returnObject;
            }
            
            returnObject.Data = new VmGetTitlesByNameSearch();
            try
            {
                
                IQueryable<Title> titles =
                    _dbTeTitlesContext.Title
                        .Where(t =>
                            t.TitleName.ToLower()
                                .Contains(requestFmGetTitlesByNameSearch.TextToSearch.ToLower()
                                )
                        ).Select(t => new BLL.Titles.Models.Title
                        {
                            Name = t.TitleName,
                            StoryLine = t.StoryLine.FirstOrDefault().Description,
                            Id = t.TitleId,
                            ReleaseYear = t.ReleaseYear
                        });

                returnObject.Data.Titles = titles.ToList();
                returnObject.IsSuccess = true;
                
            }
            catch (Exception e)//Error Code 10051
            {
                returnObject.Error.ErrorMessage = "Error fetching titles. Please contact customer service.";
                returnObject.Error.ErrorCode = "10051";
            }

            return returnObject;

        }

        public JsonWrapper<VmGetTitleDetailsByTitleId> GetTitleDetailsByTitleId(FmGetTitleDetailsByTitleId request)
        {
            JsonWrapper<VmGetTitleDetailsByTitleId> returnObject = new JsonWrapper<VmGetTitleDetailsByTitleId>();
            returnObject.Data = new VmGetTitleDetailsByTitleId();
            returnObject.Error = new ApiError();
            returnObject.IsSuccess = false;

            try
            {
                
                IQueryable<DAL.Title> titleQueryable = _dbTeTitlesContext.Title.Where(t => t.TitleId == request.Id);
                if (titleQueryable.Any())
                {
                    DAL.Title title = titleQueryable.Single();

                    TitleWithDetails titleWithDetails = new TitleWithDetails
                    {
                        Name = title.TitleName,
                        ReleaseYear = title.ReleaseYear,

                        Genres = title.TitleGenre
                            .Select(t => new TitleGenre
                            {
                                Name = t.Genre.Name
                            })
                            .ToList(),

                        Awards = title.Award
                            .Select(t => new TitleAward
                            {
                                Award = t.Award1,
                                AwardYear = t.AwardYear,
                                AwardWon = t.AwardWon,
                                AwardCompany = t.AwardCompany
                            })
                            .ToList(),

                        OtherNames = title.OtherName
                            .Select(t => new TitleOtherName
                            {
                                TitleNameLanguage = t.TitleNameLanguage,
                                TitleNameType = t.TitleNameType,
                                TitleName = t.TitleName,
                                TitleNameSortable = t.TitleNameSortable
                            })
                            .ToList(),

                        Participants = title.TitleParticipant
                            .Select(t => new TitleParticipant
                            {
                                Name = t.Participant.Name,
                                ParticipantType = t.Participant.ParticipantType,
                                IsKey = t.IsKey,
                                RoleType = t.RoleType,
                                IsOnScreen = t.IsOnScreen
                            })
                            .ToList(),

                        StoryLines = title.StoryLine
                            .Select(t => new TitleStoryLine
                            {
                                Type = t.Type,
                                Language = t.Language,
                                Description = t.Description
                            })
                            .ToList()
                    };
                    returnObject.Data.Title = titleWithDetails;
                    returnObject.IsSuccess = true;
                }

            }
            catch (Exception e)//ERROR 100136
            {
                returnObject.Error.ErrorMessage = "Error fetching details.  Please try another title.";
                returnObject.Error.ErrorCode = "100136";
            }
            

            return returnObject;
        }

    }
}
