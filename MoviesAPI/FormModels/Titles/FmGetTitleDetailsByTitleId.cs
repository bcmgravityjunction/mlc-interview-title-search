﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.FormModels.Titles
{
    public class FmGetTitleDetailsByTitleId
    {
        public int Id { get; set; }
    }
}
