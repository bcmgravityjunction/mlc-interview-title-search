﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MoviesAPI.BLL.Titles;
using MoviesAPI.Common;
using MoviesAPI.DAL;
using MoviesAPI.FormModels.Titles;
using MoviesAPI.ViewModels.Titles;

namespace MoviesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TitlesController : ControllerBase
    {
        private TitlesHandler _titlesHandler;

        public TitlesController(TitlesHandler titlesHandler)
        {
            _titlesHandler = titlesHandler;
        }
        

        [HttpPost("GetTitlesByNameSearch")]
        public JsonWrapper<VmGetTitlesByNameSearch> GetTitlesByNameSearch([FromBody] FmGetTitlesByNameSearch requestFmGetTitlesByNameSearch)
        {
            
            return _titlesHandler.GetTitlesByNameSearch(requestFmGetTitlesByNameSearch);
        }

        [HttpPost("GetTitleDetailsByTitleId")]
        public JsonWrapper<VmGetTitleDetailsByTitleId> GetTitleDetailsByTitleId([FromBody] FmGetTitleDetailsByTitleId requestFmGetTitlesByNameSearch)
        {
            return _titlesHandler.GetTitleDetailsByTitleId(requestFmGetTitlesByNameSearch);
        }
    }
}
